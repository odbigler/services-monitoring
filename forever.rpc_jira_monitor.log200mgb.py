#Author: Oded Bigler

import pexpect # Pip install pexpect on failures
import time, sys, glob, socket, traceback, smtplib, stat, re, os, signal
import socket, select
import logging, zipfile, fileinput
import logging.handlers as handlers

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders

class SizedTimedRotatingFileHandler(handlers.TimedRotatingFileHandler):
    """
    Handler for logging to a set of files, which switches from one file
    to the next when the current file reaches a certain size, or at certain
    timed intervals
    """
    def __init__(self, filename, mode='a+', maxBytes=0, maxBackupBytes=0, backupCount=0, encoding=None, delay=0, when='h', interval=1, utc=False):
        if maxBytes > 0:
            mode = 'a+'
        handlers.TimedRotatingFileHandler.__init__(
            self, filename, when, interval, backupCount, encoding, delay, utc)
        self.maxBytes = maxBytes
        self.maxBackupBytes = maxBackupBytes

    def shouldRollover(self, record):
        """
        Determine if rollover should occur.

        Basically, see if the supplied record would cause the file to exceed
        the size limit we have.
        """
        if self.stream is None:                 # delay was set...
            self.stream = self._open()
        if self.maxBytes > 0:                   # are we rolling over?
            msg = "%s\n" % self.format(record)
            self.stream.seek(0, 2)  #due to non-posix-compliant Windows feature
            if self.stream.tell() + len(msg) >= self.maxBytes:
                return 1
        t = int(time.time())
        if t >= self.rolloverAt:
            return 1
        return 0
   
    def doRollover(self):
        """
        do a rollover; in this case, a date/time stamp is appended to the filename
        when the rollover happens.  However, you want the file to be named for the
        start of the interval, not the current time.  If there is a backup count,
        then we have to get a list of matching filenames, sort them and remove
        the one with the oldest suffix.
        """
        self.stream.close()
        # get the time that this sequence started at and make it a TimeTuple
        t = self.rolloverAt - self.interval
        timeTuple = time.localtime(t)
        dfn = self.baseFilename + "." + time.strftime(self.suffix, timeTuple)
        if os.path.exists(dfn):
            os.remove(dfn)
        os.rename(self.baseFilename, dfn)
        if not os.path.exists(dfn):
            # Failed to rename file... try to copy it
            with file(self.baseFilename, 'rb') as f:
                bb=f.read()
            with file(dfn, 'wb') as f:
                f.write(bb)
            assert os.path.exists(dfn)

        if self.backupCount > 0:
            # find the oldest log file and delete it
            s = glob.glob(self.baseFilename + ".*")
            
            if sum([os.stat(f).st_size for f in s]) > self.maxBackupBytes:
                s=sorted(s, key=lambda f: os.stat(f).st_ctime)
                if s[0] != dfn:
                    os.remove(s[0])

        self.stream = open(self.baseFilename, 'wb')

        self.rolloverAt = self.rolloverAt + self.interval
        if os.path.exists(dfn + ".zip"):
            os.remove(dfn + ".zip")
        file = zipfile.ZipFile(dfn + ".zip", "wb")
        try:
            file.write(dfn, os.path.basename(dfn), zipfile.ZIP_DEFLATED)
        except:
            pass
        file.close()
        os.remove(dfn)

mode, logName, size, _ = __file__.split('.')
forever = mode == 'forever'
    
size = re.match('log(\d+)mgb', size).groups()[0]
backupSize = size#re.match('(\d+)M', backupSize).groups()[0]
fileCount=0xfffff

logger = logging.getLogger('MyLogger')
logger.setLevel(logging.DEBUG)

directory = os.path.realpath(os.path.join(os.path.dirname(__file__), 'log'))
if os.path.exists(directory) is False:
    os.mkdir(directory)

handler=SizedTimedRotatingFileHandler(
    os.path.join(directory, logName+'.log'), maxBytes=int(size)*1024*1024, maxBackupBytes=int(backupSize) * 1024 * 1024, backupCount=int(fileCount),
    when='MIDNIGHT',interval=1)

logger.addHandler(handler)

cmd_command = ' '.join(sys.argv[1:])


def send_mail(ret, signalstatus, message_lines):
    gmail_user = 'KyteraMailer@gmail.com'
    gmail_pwd = 'nbGu8Db3k1'
    
    try:
        msg = MIMEMultipart('multipart')
        msg['From'] = gmail_user
        to = ['kyteranotify@gmail.com']
        msg['To'] = ','.join(to)

        msg['Subject'] = "Command Crashed %s %s exit code: %d" % (socket.gethostname(), cmd_command, ret)
        
        message_ansi = os.linesep.join(["exit_code:%d signal_status:%s" % (ret, str(signalstatus))] + message_lines[::-1])
        
        try:
            import ansi2html
            conv = ansi2html.Ansi2HTMLConverter()
            html_output = conv.convert(message_ansi)
            part = MIMEBase('application', "octet-stream")
            part.set_payload(html_output)
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="log.html"')
            msg.attach(MIMEText(conv.convert(message_ansi[:300]), 'html'))
            msg.attach(part)
        except:
            message_ansi = "please pip install ansi2html to get html colored message on %s.\n"%socket.gethostname()+message_ansi[:200]
            msg.attach(MIMEText(message_ansi, 'plain'))

        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server_ssl.ehlo() # optional, called by login()
        server_ssl.login(gmail_user, gmail_pwd)
        # ssl server doesn't support or need tls, so don't call server_ssl.starttls() 

        server_ssl.sendmail(gmail_user, to, msg.as_string())
        server_ssl.quit()
        server_ssl.close()
    except:
        traceback.print_exc()

while True:
    print 'Running', cmd_command
    global restart_requested
    restart_requested = False
    start_time = time.time()
	
    child = pexpect.spawn(cmd_command,timeout=None)

    def stop_process(*a):
        print 'Forever: Got SIGINT'
        interupted = True
        child.close(True)
        os._exit(0)

    def restart_lmuapp(*a):
        global restart_requested
        restart_requested = True
        print 'Restart requested', restart_requested
        logger.debug('Restart Requested')

    signal.signal(signal.SIGINT, stop_process)
    signal.signal(signal.SIGUSR1, restart_lmuapp) 

    while restart_requested is False:
    
        try:
            child.expect('\n', timeout = 1)
            print(child.before)
            logger.debug(child.before)
        except pexpect.TIMEOUT:
            continue
        except pexpect.EOF:
            break

    if restart_requested is True:
        #print 'Terminating process'
        logger.debug('Terminating process')
        child.terminate(True)
        child.close()

    # Finished
    logger.info('Forever: process finished: %s %s', child.exitstatus, child.signalstatus)
    if restart_requested is False and child.exitstatus != 0 and child.exitstatus != None:
		if time.time() - start_time > 30*60: # The script crashed after more then half hour.
			with file( os.path.join(directory, logName+'.log'),'rb') as f:
				send_mail(child.exitstatus or 0xff, child.signalstatus, f.readlines()[-1000:] )
    if not forever: # Otherwise do re running
        break

