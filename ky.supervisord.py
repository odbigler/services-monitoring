#!/usr/bin/env python
import supervisor.loggers

import time, glob, os, sys, traceback, subprocess

import zipfile
import logging.handlers as handlers
import re

class SizedTimedRotatingFileHandler(handlers.TimedRotatingFileHandler):
    """
    Handler for logging to a set of files, which switches from one file
    to the next when the current file reaches a certain size, or at certain
    timed intervals
    """

    def __init__(self, filename, mode='a+', maxBytes=0xffffff, maxBackupBytes=512 * 1024 * 1024,
                 backupCount=0xffff, encoding=None, delay=0, when='MIDNIGHT', interval=1, utc=False): # 'MIDNIGHT'
        if maxBytes > 0:
            mode = 'a+'
        handlers.TimedRotatingFileHandler.__init__(

            self, filename, when, interval, 0xffff, encoding, delay, utc) # backupCount = 0xfffff
        self.maxBytes = maxBytes
        self.maxBackupBytes = maxBackupBytes

    def setFormat(self, fmt):
        pass

    def shouldRollover(self, record):
        """
        Determine if rollover should occur.

        Basically, see if the supplied record would cause the file to exceed
        the size limit we have.
        """
        t = int(time.time())
        if t >= self.rolloverAt:
            return 1
        return 0

    def doRollover(self):
        """
        do a rollover; in this case, a date/time stamp is appended to the filename
        when the rollover happens.  However, you want the file to be named for the
        start of the interval, not the current time.  If there is a backup count,
        then we have to get a list of matching filenames, sort them and remove
        the one with the oldest suffix.
        """
        self.stream.close()
        # get the time that this sequence started at and make it a TimeTuple
        t = self.rolloverAt - self.interval
        timeTuple = time.localtime(t)
        dfn = self.baseFilename + "." + time.strftime(self.suffix, timeTuple)
        if os.path.exists(dfn):
            os.remove(dfn)
        os.rename(self.baseFilename, dfn)
        if not os.path.exists(dfn):
            # Failed to rename file... try to copy it
            with file(self.baseFilename, 'rb') as f:
                bb=f.read()
            with file(dfn, 'wb') as f:
                f.write(bb)
            assert os.path.exists(dfn)

        if self.backupCount > 0:
            # find the oldest log file and delete it
            s = glob.glob(self.baseFilename + ".*")
            
            if sum([os.stat(f).st_size for f in s]) > self.maxBackupBytes:
                s=sorted(s, key=lambda f: os.stat(f).st_ctime)
                if s[0] != dfn:
                    os.remove(s[0])

        self.stream = open(self.baseFilename, 'wb')

        self.rolloverAt = self.rolloverAt + self.interval
        if os.path.exists(dfn + ".zip"):
            os.remove(dfn + ".zip")
        file = zipfile.ZipFile(dfn + ".zip", "wb")
        try:
            file.write(dfn, os.path.basename(dfn), zipfile.ZIP_DEFLATED)
        except:
            pass
        file.close()
        os.remove(dfn)

def getLogger(filename, level, fmt, rotating=False, maxbytes=0, backups=0, stdout=False):
    handlers = []

    if filename.endswith('supervisord.log'):
        return old_get_logger(filename, level, fmt, rotating, maxbytes, backups, stdout)

    logger=logging.getLogger('ky.supervisor_'+filename)
    logger.setLevel(level)
    logger.trace = logger.debug
    logger.blather = lambda *a, **kw: None

    def close(*a):
        for handler in logger.handlers:
            handler.close()
            del handler
        logger.handlers=[]

    logger.close = close
    logger.close()

    import coloredlogs
    coloredlogs.install()
    if filename is None:
        if not maxbytes:
            maxbytes = 1<<21 #2MB
        io = BoundIO(maxbytes)
        handlers.append(StreamHandler(io))
        logger.getvalue = io.getvalue

    elif filename == 'syslog':
        handlers.append(SyslogHandler())

    else:
        if rotating is False:
            handlers.append(FileHandler(filename))
        else:
            handlers.append(SizedTimedRotatingFileHandler(filename,'a',maxbytes,backups))

    if stdout:
        handler=supervisor.loggers.StreamHandler(sys.stdout)
        handler.handle = handler.emit
        handlers.append(handler)

    for handler in handlers:
        handler.setFormat(fmt)
        handler.setLevel(level)
        logger.addHandler(handler)

    return logger

old_get_logger = supervisor.loggers.getLogger
supervisor.loggers.getLogger = getLogger

import supervisor.dispatchers
from supervisor.dispatchers import POutputDispatcher as POutputDispatcher

#from supervisor.rpcinterface import RPas POutputDispatcher

class kyPOutputDispatcher(POutputDispatcher):
    def __init__(self, process, event_type, fd):
        POutputDispatcher.__init__(self, process, event_type, fd)
        self.log_to_mainlog=False
supervisor.dispatchers.POutputDispatcher = kyPOutputDispatcher

from supervisor.rpcinterface import SupervisorNamespaceRPCInterface
import supervisor.rpcinterface

g_descriptionHook = {}

class kySupervisorNamespaceRPCInterface(SupervisorNamespaceRPCInterface):
    def __init__(self, supervisord):
        SupervisorNamespaceRPCInterface.__init__(self, supervisord)

    def _update(self, text):
        import inspect

        if text != 'getProcessInfo':
            msg = "kytera hook RPC call: %s" % text
            for (frame, filename, linenum, functionName, lines, index) in inspect.getouterframes(inspect.currentframe())[1:4]:
                msg+= "\n%s %s %s " % (filename, functionName, linenum)
            msg += "\n~End of kytera hook %s ~" % text
            self.supervisord.options.logger.info(msg)

        return SupervisorNamespaceRPCInterface._update(self, text)

    def getProcessInfo(self, name):
        group, process = self._getGroupAndProcess(name)
        #if process.config.directory:
        #    directory = process.config.directory
        #else:
        directory = [os.path.dirname(p) for p in process.config.command.split(' ') if p.endswith('.py') or p.endswith('.js') ]
        directory = directory[0] if directory else process.config.directory 
        if not os.path.isabs(directory):
            directory=os.path.join(process.config.directory, directory)

        try:
            import release_manager
            info = release_manager.get_basic_info(directory)
            version = subprocess.check_output(['git', 'describe', '--dirty'], cwd=directory)
            release_msg = subprocess.check_output("git for-each-ref --sort=-taggerdate --count=1 --format '%(contents:subject)'  refs/tags".split(' '), cwd=directory)
        
            try:
                x['repo_name'] = subprocess.check_output('git remote -v'.split(' ')).split(os.linesep)[0].split('@')[1].split(' ')[0].replace('bitbucket.org:','')
            except:
                import traceback
                self.supervisord.options.logger.debug("No repo Name: " + traceback.format_exc())

            projName=info.get('repo_name','').split('/')[-1]
            description=u'  \n<a title="%s" href="http://10.8.1.1/doku.php?id=services_releases#%s">%s</a>'%('Last commit: ' + info.get('msg', '') , projName, '%s' % (projName))
            description+=u'  <a title="%s"  href="https://bitbucket.org/%s/src/%s%s">%s</a>'%('Release: '+ release_msg ,info.get('repo_name', '').replace('.git',''), info['commit'], '?at=%s'%info['branch'] if 'branch' in info else '' , version)
        except:
            import traceback
            self.supervisord.options.logger.debug(traceback.format_exc())
            description=''

        # Try to find release info.
        self.supervisord.options.logger.debug("kytera hook called with getProcessInfo")
        ret = SupervisorNamespaceRPCInterface.getProcessInfo(self, name)
        self.supervisord.options.logger.debug("Replaceing description for %s %s %s" % (ret['name'], ret['description'], description))
        if 'pid' in ret['description']:
            g_descriptionHook[ret['name']] = (ret['description'], description.encode('utf-8'))
        
        return ret

supervisor.rpcinterface.SupervisorNamespaceRPCInterface=kySupervisorNamespaceRPCInterface

from supervisor.web import StatusView, TailView

import supervisor.web

class kyStatusView(StatusView):
    def render(self):
        ret = StatusView.render(self)
        if ret == supervisor.web.NOT_DONE_YET:
            return ret
        ret2=ret

        for (old_description, new_description) in g_descriptionHook.values():
            ret2=ret2.replace(old_description, (old_description + new_description) )

        return ret2


COLOR_DICT = {
    '31': [(255, 30, 30), (128, 0, 0)],
    '32': [(70, 150, 0), (0, 128, 0)],
    '33': [(200, 150, 0), (128, 128, 0)],
    '34': [(30, 50, 255), (0, 0, 128)],
    '35': [(160, 30, 160), (128, 0, 128)],
    '36': [(30, 200, 170), (0, 128, 128)],
}

COLOR_REGEX = re.compile(r'\[(?P<arg_1>\d+)(;(?P<arg_2>\d+)(;(?P<arg_3>\d+))?)?m')

BOLD_TEMPLATE = '<span style="color: rgb{}; font-weight: bolder">'
LIGHT_TEMPLATE = '<span style="color: rgb{}">'

def ansi_to_html(text):
    text = text.replace('[m', '</span>')
    text = text.replace('\x1b', '</span>')

    def single_sub(match):
        argsdict = match.groupdict()
        if argsdict['arg_3'] is None:
            if argsdict['arg_2'] is None:
                color, bold = argsdict['arg_1'], 0
            else:
                color, bold = argsdict['arg_1'], int(argsdict['arg_2'])
        else:
            color, bold = argsdict['arg_2'], int(argsdict['arg_3'])

        if color not in COLOR_DICT:
            color = '35'

        if bold:
            return BOLD_TEMPLATE.format(COLOR_DICT.get(color)[1])
        return LIGHT_TEMPLATE.format(COLOR_DICT[color][0])

    return COLOR_REGEX.sub(single_sub, text)


class KyTailView(TailView):
    def render(self):
        self.context.form.setdefault('limit', '100024')

        ret = TailView.render(self)
        return ansi_to_html(ret)

supervisor.web.StatusView = kyStatusView
supervisor.web.VIEWS['index.html']['view']=kyStatusView

supervisor.web.TailView = KyTailView
supervisor.web.VIEWS['tail.html']['view']=KyTailView


if __name__ == "__main__":
    import supervisor
    import supervisor.supervisord
    import logging

    supervisor.supervisord.main()
