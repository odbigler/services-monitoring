#!/usr/bin/python
# fix phy config token
from argparse import ArgumentParser
import subprocess, shutil, os
from pymongo import MongoClient
from datetime import datetime
import socket
import git
import pytz
import humanize
import pprint
pp = pprint.PrettyPrinter(indent=4)

def get_basic_info(path='.'):
    r = git.Repo(path, search_parent_directories=True)
    x={'repo': r, 
        'is_dirty':r.is_dirty(index=False, working_tree=True, untracked_files=False, submodules=True),
        'commit':r.head.object.hexsha, 
        'msg': r.head.object.message,
        'source_upate_time' : r.iter_commits().next().authored_datetime,}
    try:
        x['branch']= r.active_branch.name
    except:
        pass
    try:
        x['repo_name'] = subprocess.check_output('git remote -v'.split(' ')).split(os.linesep)[0].split('@')[1].split(' ')[0].replace('bitbucket.org:','')
    except:
        import traceback
        traceback.print_exc()
        try:
            x['repo_name'] = r.remotes.origin.url.split('bitbucket.org/')[1]
        except:
            pass

    return x

def fetch_last_release(info):
    mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
    last_featch = list(mongo_client['companion']['releases'].find({'repo_name':info['repo_name'], "branch" : info["branch"], 'source_upate_time': {'$lte': info['source_upate_time']}}).sort('source_upate_time', -1).limit(0) )
    if last_featch:
        return last_featch[0]
    else:
        return None
    
def find_named_version_version(info, max_lookup=200):
    r=info['repo']
    
    last_release = fetch_last_release(info)# {'version':'1.2', 'commit':'eaf95b3144847f009ca22d9178ce229e320a207d'} #fetch_last_release(info['repo_name'], info['branch'])
    
    it = r.iter_commits()
    
    if last_release:
        counter = 0
        while counter < max_lookup:
            try:
                commit = it.next()
                if commit.binsha.encode('hex') == last_release['commit']:
                    last_release_date = commit.authored_datetime
                    break
                counter += 1
            except StopIteration:
                counter = -1
                break
                
        if counter == max_lookup:
            counter = -1
            
    if last_release and counter != -1:
        info['named_version'] = last_release
        info['commits_after_named_version']=counter
        info['named_version_date']=last_release_date
    else:
        info['named_version']=None
        info['commits_after_named_version']=-1
        info['named_version_date']=None
        
def pretty_description(info):
    date_str = humanize.time.naturaldate(info['source_upate_time'])
    if info['named_version']:
        description = info['named_version']['release_version'] + ' ' + humanize.time.naturaldate(info['named_version_date'])
        if info['commits_after_named_version']:
            description += '(+%d commits %s)' % (info['commits_after_named_version'], date_str)
    else:
        description = info['repo'].git.describe() 

    if info['is_dirty']:
        description = 'Dirty ' + description
    info['description'] = description

def get_current_version(info):
    find_named_version_version(info)
    pretty_description(info)

def generate_release(info, release_version, msg):
    assert msg, 'Please provide msg with -m or --msg when releasing new verison'
    if info['is_dirty']:
        default_args = ['--abbrev=40', '--full-index', '--raw'] 
        diff=release['repo'].git.diff(*default_args)
        assert info['is_dirty'] is False, 'cannot release dirty repo. Clean your repo!!\nDiff is %s'%diff
    
    release=info.copy()
    del release['repo']
    release.update({'type':'release', 'msg': msg,'release_version': release_version, 'release_generation': datetime.utcnow(), 'release_computer': socket.gethostname(),})
     
    mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
    past_release = mongo_client['companion']['releases'].find_one({'repo_name':release['repo_name'], 'release_version': release['release_version']})
    if past_release is not None:
        print 'Version %s is already exists' % (release['release_version'])
        print
        pp.pprint(past_release)
        return
    new_tag=info['repo'].create_tag(release_version, messages = msg)
    repo.remotes.origin.push(new_tag)
    mongo_client['companion']['releases'].insert(release)

def force_checkout(info, version):
    mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
    release = mongo_client['companion']['releases'].find_one({'repo_name':info['repo_name'], 'release_version': release['version']})
    branch = release['branch']
    assert release, 'Failed to find release with version %s' % version
    
    assert not os.system('git fetch https://kyuser:Kytera2014@bitbucket.org/kytera/lmuapp-rpi.git +refs/heads/%s:refs/remotes/origin/%s' % (branch, branch))
    assert not os.system('git branch %s --set-upstream origin/%s' % (branch, branch))
    assert not os.system('git checkout --force %s' % branch)
    assert not os.system('git reset --hard %s' % info['commit'])

def diff_to_release(info, version):
    if version:
        release = mongo_client['companion']['releases'].find_one({'repo_name':info['repo_name'], 'release_version': version})
        assert release, 'No such release with version %s' % version
        last_release = release
    else:
        last_release = fetch_last_release(info)

    os.system('git diff %s' % last_release['commit'] )

def main():
    ap = ArgumentParser("Release Manager", 'Either:\n1. No arguments-Get information about current version\n2.--release VERSION Generate a new release. Example --release 1.5.2.1 \n3. --force-checkout Force a git checkout of a new release. Example: --force-checkout 1.5.2.1.\n4. --diff Generate diff against last version. ')
    ap.add_argument('--release',metavar='VERSION', help='Generate a new release')
    ap.add_argument('-m','--msg', default='', help='Describe the release. Whats the new feature/bugfix or just quick hotfix?')
    ap.add_argument('--force-checkout', metavar='VERSION', help='Generate a new release')
    ap.add_argument('--diff', action="store", default='', metavar='VERSION', help='Generate a new release')

    options = ap.parse_args()
    
    info = get_basic_info() 
    
    if options.release:
        generate_release(info, options.release, options.msg)
    elif options.force_checkout:
        force_checkout(info, options.force_checkout)
    elif options.diff:
        diff_to_release(info, options.version)
        return
    print
    #print "Current version: [%s]"%info['description']
    print
    pp.pprint(info)
     
    projName=info['repo_name'].split('/')[-1]
    #description='<a title="%s" href="http://10.8.1.1/doku.php?id=releases:%s#%s">%s</a>'%('Last commit msg: ' + info['msg'] , projName , info['named_version']['release_version'], '%s/%s' % (projName, info['branch']))
    #description+='  <a title="%s"  href="https://bitbucket.org/%s/src/%s?at=%s">%s</a>'%('Release msg: ' + info['named_version'].get('msg','') ,info['repo_name'].replace('.git',''), info['commit'], info['branch'],info['description'])
    #print description



if __name__ == "__main__":
    main()
