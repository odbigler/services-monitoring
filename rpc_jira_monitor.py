#!/usr/bin/env python

__author__ = 'Elad Sofer <elad.sofer@kyteratech.com>'


import socket
import time
import coloredlogs
import re
from enum import Enum
from datetime import datetime, timedelta
from logging import getLogger

import traceback
import jira
from ConfigParser import ConfigParser, NoOptionError
from xmlrpclib import Server
from pymongo import MongoClient

from supervisor.options import Options


class IssueStatus(Enum):
    TO_DO = 'TO DO'
    DONE = 'DONE'


class JiraNotifier(object):

    ALERT_INTERVAL = 30*60
    SAMPLE_RATE = 0.5

    def __init__(self):
        self.conf = ConfigParser()
        # self.conf.readfp(open(Options().default_configfile()))
        self.conf.readfp(open('/etc/ky.supervisord.conf'))

        self.jira = jira.JIRA(options={'server': r'https://kyteratech.atlassian.net'},
                              basic_auth=('KyteraMailer2', 'nbGu8Db3k1'))

        rpc_port = re.findall(r'\d+', self.conf.get('inet_http_server', 'port'))[0]
        self.rpc_client = Server('http://localhost:{port}/RPC2'.format(port=rpc_port))

        self.mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,
        ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
        self.heart_b_coll = self.mongo_client.events.heartbeat

        self.hostname = socket.gethostname()
        self.services = [section.split(':')[1] for section in
                         self.conf.sections() if 'program' in section]

        self.cached_issues = {s: dict() for s in self.services}

        self.last_z_heart_b = dict.fromkeys(self.services, None)
        self.services_heart_b = dict.fromkeys(self.services)
        self.services_watchers = dict.fromkeys(self.services)

        self.logger = getLogger(__name__)
        coloredlogs.install(level='DEBUG', logger=self.logger)

    def start_monitoring(self):

        # Initialize service vs issues dictionary
        self.init_service_statues()

        while True:
            try:
                processes = self.rpc_client.supervisor.getAllProcessInfo()
            except Exception:
                traceback.print_exc()
                time.sleep(self.SAMPLE_RATE)
                continue

            time_delta = datetime.utcnow() - timedelta(minutes=6)
            docs = list(self.heart_b_coll.find({"timestamp": {'$gte': time_delta}, "host": self.hostname}))

            for p in processes:
                if self.services_heart_b[p['name']]:
                    actual_hb = {doc['name'] for doc in docs if
                                 doc['name'] in self.services_heart_b[p['name']]}
                    found_all_heart_b = self.services_heart_b[p['name']].issubset(actual_hb)
                    heartbeat_iss = self.cached_issues[p['name']]['heartbeat']
                    heart_b_status = heartbeat_iss.raw['fields']['status']['name']

                status_iss = self.cached_issues[p['name']]['status']
                issue_status = status_iss.raw['fields']['status']['name']

                if p['statename'] == 'RUNNING' and time.time() - p['start'] > self.ALERT_INTERVAL:
                    # Alert if the issue isn't on DONE status
                    if issue_status.upper() != IssueStatus.DONE.value:
                        self.change_issue_status(status_iss, IssueStatus.DONE, p['name'],
                                                 service_status=p['statename'])

                    if self.services_heart_b[p['name']]:
                        if found_all_heart_b and heart_b_status.upper() != IssueStatus.DONE.value:
                            last_z_alert = self.last_z_heart_b[p['name']]
                            if last_z_alert:
                                if time.time() - last_z_alert > self.ALERT_INTERVAL:
                                    self.change_issue_status(heartbeat_iss, IssueStatus.DONE, p['name'],
                                                             heartbeats=actual_hb)
                                else:
                                    last_z = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(last_z_alert))
                                    heartbeat_iss.update(description='There is no heartbeat since {0}'.format(last_z))
                            else:
                                self.change_issue_status(heartbeat_iss, IssueStatus.DONE, p['name'],
                                                         heartbeats=actual_hb)

                        # Alert if the process is in a zombie state
                        if not found_all_heart_b and heart_b_status.upper() != IssueStatus.TO_DO.value:
                            self.last_z_heart_b[p['name']] = time.time()

                            self.change_issue_status(heartbeat_iss, IssueStatus.TO_DO, p['name'],
                                                     heartbeats=actual_hb)
                    # Alert if someone stopped the process for longer then the interval
                elif p['statename'] == 'STOPPED' and time.time() - p['stop'] > self.ALERT_INTERVAL:
                    if issue_status.upper() != IssueStatus.TO_DO.value:
                        self.change_issue_status(status_iss, IssueStatus.TO_DO, p['name'],
                                                 service_status=p['statename'])
                    # Alert on an EXIT
                elif p['statename'] == 'EXITED' or p['statename'] == 'FATAL' or p['statename'] == 'STARTING':
                    if issue_status.upper() != IssueStatus.TO_DO.value:
                        self.change_issue_status(status_iss, IssueStatus.TO_DO, p['name'],
                                                 service_status=p['statename'])

            time.sleep(self.SAMPLE_RATE)

    def init_service_statues(self):

        for service in self.services:
            self.services_heart_b[service] = self.get_service_heartbeat_list(service)
            self.services_watchers[service] = self.get_service_watchers(service)

        i_kinds = ['heartbeat', 'status']
        for service in self.services:
            for kind in i_kinds:

                if kind == 'heartbeat' and not self.services_heart_b[service]:
                    self.cached_issues[service][kind] = None
                    continue

                generator = '{0}_{1}_{2}'.format(self.hostname, service, kind)
                issues = self.jira.search_issues('project="SM" AND generator="{0}"'.format(generator))
                if not issues:
                    if kind == 'status':
                        summary = 'service {0} crashed on server {1}'.format(service, self.hostname)
                        description = '{0} >>> Issue which describes service {1} {2}'.format(str(datetime.utcnow()),
                                                                                             service, kind)
                    else:
                        summary = 'No heartbeat from service {0} on server {1}'.format(service, self.hostname)
                        description = '{0} >>> service {1} is running but there is no {2}'.format(str(datetime.utcnow()),
                                                                                                  service, kind)

                    watchers = self.services_watchers[service]
                    self.cached_issues[service][kind] = self.create_jira(watchers, generator,
                                                                         service, summary, description)

                    self.logger.info('{0} Issue {1} was created for generator {2}'.format(kind,
                                                                                          self.cached_issues[service][kind].key,
                                                                                          generator))
                else:
                    self.cached_issues[service][kind] = issues[0]
                    self.logger.info('Found Issue {0} with generator {1}'.format(issues[0].key, generator))

    def get_service_watchers(self, service):
        try:
            watchers = self.conf.get('program:{0}'.format(service), 'recipients').split(',')
            return [recp.strip() for recp in watchers]
        except NoOptionError:
            self.logger.warn('No recipients section for service {0}'.format(service))
            return []

    def get_service_heartbeat_list(self, service):
        try:
            heartbeats = self.conf.get('program:{0}'.format(service), 'heartbeats').split(',')
            return {heartbeat.strip() for heartbeat in heartbeats}
        except NoOptionError:
            self.logger.warn('No heartbeat section for service {0}'.format(service))
            return set()

    def create_jira(self, watchers, generator, service_name, summary, description=''):
        try:
            fields = {'customfield_10057': [self.hostname], 'customfield_10046': [generator],
                      'assignee': {'name': 'KyteraMailer2'}, 'customfield_10058': [service_name]}

            c = self.jira.create_issue(project="SM", summary=summary,
                                       description=description, issuetype={'name': 'Task'},
                                       **fields)

            for watcher in watchers:
                # Oded user registered as admin
                watcher = 'admin' if watcher == 'oded' else watcher
                self.jira.add_watcher(c, watcher)

            return c

        except:
            traceback.print_exc()

    def change_issue_status(self, issue, status, service_name, service_status=None,
                            heartbeats=None):
        try:

            self.jira.transition_issue(issue, status.value)
        except:
            traceback.print_exc()
        else:
            issue_type = 'status' if heartbeats is None else 'heartbeat'

            if issue_type == 'status' and service_status:
                msg = '{0} >>> supervisord service {1} status is {2}'.format(str(datetime.utcnow()),
                                                                             service_name, service_status)
                self.logger.info(msg)
                issue.update(description=msg)

            else:
                h_b_diff = str(self.services_heart_b[service_name].difference(heartbeats))

                msg = '{0} >>> supervisord service {1} has the following heartbeats'
                ': {2}\nmissing heartbeats: {3}'.format(str(datetime.utcnow()), service_name,
                                                        str(heartbeats), h_b_diff)
                issue.update(description=msg)

            if status == IssueStatus.DONE:
                self.cached_issues[service_name][issue_type].raw['fields']['status']['name'] = IssueStatus.DONE.value
            else:
                self.cached_issues[service_name][issue_type].raw['fields']['status']['name'] = IssueStatus.TO_DO.value


def main():
    notifier = JiraNotifier()
    notifier.start_monitoring()


if __name__ == '__main__':
    main()
